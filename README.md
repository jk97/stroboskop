# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://jk97@bitbucket.org/jk97/stroboskop.git
cd stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/jk97/stroboskop/commits/50c81429d9522805b73a26179d89eed06568fb2a

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/jk97/stroboskop/commits/d853bc27aba3b14b83985285b42db9c0f6d5efbe

Naloga 6.3.2:
https://bitbucket.org/jk97/stroboskop/commits/672cbd1580d8b2db6b314354becf00fd1fde32b6

Naloga 6.3.3:
https://bitbucket.org/jk97/stroboskop/commits/4f064f9b10fb9c66da12fde0293ee3e43cb8c4fc

Naloga 6.3.4:
https://bitbucket.org/jk97/stroboskop/commits/54882a3bf758bf89fe6dd5dca59a7e37de4a1f08

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/jk97/stroboskop/commits/9b4a8c267cb3f461a7d9b3ff0df150bcb6907e00

Naloga 6.4.2:
https://bitbucket.org/jk97/stroboskop/commits/c77cbb9c90ac13d64caa08d981f5ad064891389b

Naloga 6.4.3:
https://bitbucket.org/jk97/stroboskop/commits/0562f494f793bf25362a29bb8d0e4a2efa6d1d14

Naloga 6.4.4:
https://bitbucket.org/jk97/stroboskop/commits/7ebc366bd51e0edf0c1246b171107585f499f71c